﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	class WomanOver60 : IBillPayer
	{

		public void CalculateBill(double amountDue)
		{
			Console.WriteLine("Bill Amount for Woman Over 60: $" + (amountDue - (amountDue * .10)));	
		}
	}

	class ManOver60 : IBillPayer
	{

		public void CalculateBill(double amountDue)
		{

			Console.WriteLine("Bill Amount for Man Over 60: $" + (amountDue - (amountDue * .05)));

		}
	}

	class ManUnder60 : IBillPayer
	{

	public void CalculateBill(double amountDue)
	{
			Console.WriteLine("Bill Amount for Man Under 60: $" + amountDue);
	}

}

}



