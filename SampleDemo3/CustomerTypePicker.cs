﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	class CustomerTypePicker
	{

		public static IBillPayer getWomanOver60()
		{

			return new WomanOver60();

		}

		public static IBillPayer getManOver60()
		{

			return new ManOver60();

		}

		public static IBillPayer getManUnder60()
		{

			return new ManUnder60();

		}

	}
}
