﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	class BLTBuilder : SandwichBuilder
	{

		// Methods that make this different from other Sandwich Objects
		Sandwich sandwich = null;
		public BLTBuilder()
		{
			sandwich = new Sandwich();
		}

	public override void buildBread()
	{

		sandwich.setBread("White Bread");

	}

	public override void buildVegetables()
	{

		sandwich.setVegetables("Lettuce Tomato");

	}

	public override void buildMeat()
	{

		sandwich.setMeat("Bacon");

	}

	public override void buildCheese()
	{

		sandwich.setCheese("White Bread");

	}

	public override void buildCondiments()
	{

		sandwich.setCondiments("Mayonnaise");

	}

}
}
