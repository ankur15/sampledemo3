﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	public class CashRegister
	{

		ICommand theCommand;

		public CashRegister(ICommand newCommand)
		{

			theCommand = newCommand;

		}

		public void returnFinalBill(double amountDue)
		{

			theCommand.ExecuteCalculateBill(amountDue);

		}

	}
}
