﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	public class Product : ProductComponent
	{

	private String productName;
	private double productPrice;

	public Product(String productName, double productPrice)
	{		
		this.productName = productName;
		this.productPrice = productPrice;
	}

	public String getProductName() { return productName; }

	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	public double getProductPrice() { return productPrice; }

	public void setProductPrice(double productPrice)
	{
		this.productPrice = productPrice;
	}

	public override void displayProductInfo()
	{

			Console.WriteLine(getProductName() + " $" + getProductPrice());

	}

}
}
