﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	class SandwichArtist
	{

		private SandwichBuilder sandwichBuilder;

		public void setSandwichBuilder(SandwichBuilder sandwichBuilder)
		{

			this.sandwichBuilder = sandwichBuilder;

		}

		public Sandwich getSandwich() { return sandwichBuilder.getSandwich(); }

		// Initializes the Sandwich object

		public void takeSandwichOrder()
		{

			sandwichBuilder.makeSandwich();
			sandwichBuilder.buildBread();
			sandwichBuilder.buildVegetables();
			sandwichBuilder.buildMeat();
			sandwichBuilder.buildCheese();
			sandwichBuilder.buildCondiments();

		}

	}
}
