﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	public class CustomerGroup
	{

		List<IBillPayer> customers;

		public CustomerGroup()
		{

			customers = new List<IBillPayer>();

		}

		public void add(IBillPayer newPayer)
		{

			customers.Add(newPayer);

		}

		public IBillPayer get(int customerIndex)
		{

			return customers[customerIndex];

		}

	}
}
