﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	public interface ICommand
	{
		 void ExecuteCalculateBill(double amountDue);

	}

	public class Waiter : ICommand
	{

		public IBillPayer thePayer;

		public Waiter(IBillPayer thePayer)
		{

			this.thePayer = thePayer;

		}


		public void ExecuteCalculateBill(double amountDue)
		{
			thePayer.CalculateBill(amountDue);
		}
	}


}
