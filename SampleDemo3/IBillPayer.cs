﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	public interface IBillPayer
	{
		void CalculateBill(double amountDue);
	}

}
