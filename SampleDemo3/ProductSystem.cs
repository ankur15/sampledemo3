﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
	class Program
	{
		static void Main(string[] args)
		{

			// Add product groups

			//	ProductGroup produce = new ProductGroup("Produce");

			//	ProductGroup cereal = new ProductGroup("Cereal");

			//	// Top level component that contains all products

			//	ProductGroup everyProduct = new ProductGroup("All Products\n");

			//// Add produce and cereal groups to everyProduct list

			//everyProduct.add(produce);
			//everyProduct.add(cereal);

			//// Add individual products to the groups

			//produce.add(new Product("Tomato", 1.99));
			//produce.add(new Product("Orange", .99));
			//produce.add(new Product("Potato", .35));

			//cereal.add(new Product("Special K", 4.79));
			//cereal.add(new Product("Cheerios", 3.68));
			//cereal.add(new Product("Raisin Bran", 3.68));

			//// Display all products sorted into groups

			//everyProduct.displayProductInfo();

			// The director has methods for assigning the
			// Sandwich to build, initializes it and provides
			// the Object to who asks for it

			//SandwichArtist paul = new SandwichArtist();

			//// Designate the specific Sandwich object we want to build

			//SandwichBuilder bltBuilder = new BLTBuilder();

			//// Assign the specific Sandwich to build

			//paul.setSandwichBuilder(bltBuilder);

			//// Initialize everything in the new object

			//paul.takeSandwichOrder();

			//// Provide the specific Sandwich object

			//Sandwich bltSandwich = paul.getSandwich();

			//// Print out info on the Sandwich Object

			//Console.WriteLine(bltSandwich.toString());

			// Get the customer to use for bill calculation

			//IBillPayer sallyMay = CustomerTypePicker.getWomanOver60();

			//// The Waiter sets the customer type so that the right 
			//// executeCalculateBill method is called

			//Waiter theWaiter = new Waiter(sallyMay);

			//// The invoker makes sure the right method is called and
			//// stores the Waiter so BillPayers assigned to Waiter
			//// are available

			//CashRegister calculateBill = new CashRegister(theWaiter);

			//// When returnFinalBill() is called that signals that the
			//// Waiter stored in CashRegister should execute method
			//// executeCalculateBill

			//calculateBill.returnFinalBill(12.00);

			//// Calculate for Man over 60

			//IBillPayer paulThumb = CustomerTypePicker.getManOver60();
			//theWaiter = new Waiter(paulThumb);
			//calculateBill = new CashRegister(theWaiter);
			//calculateBill.returnFinalBill(12.00);

			// Call commands from the BillPayer ArrayList

			CustomerGroup custGroup = new CustomerGroup();
			custGroup.add(CustomerTypePicker.getWomanOver60());
			custGroup.get(0).CalculateBill(12.00);

			Console.ReadKey();


			Console.ReadKey();
	}
	
}
}
