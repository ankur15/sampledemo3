﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDemo3
{
    public class ProductGroup : ProductComponent
    {

    List<ProductComponent> productComponents = new List<ProductComponent>();

     

    private String productGroupName;

 

    public ProductGroup(String productGroupName)
    {
        this.productGroupName = productGroupName;       
    }

 

    public void add(ProductComponent newProductComponent)
    {
        
        productComponents.Add(newProductComponent);
    }

 

    public void remove(ProductComponent newProductComponent)
    {
        
        productComponents.Remove(newProductComponent);
       
    }
    public ProductComponent getProductComponent(int componentIndex)
    {
       
        return (ProductComponent)productComponents[componentIndex];

    }
    public String getProductGroupName() { return productGroupName; }

    public override void displayProductInfo()
    {

       Console.WriteLine(getProductGroupName());

            List<ProductComponent> productIterator = productComponents;
       
            foreach(ProductComponent comp in productIterator)
            {
                comp.displayProductInfo();
            }       
        }
       
       
    }
           
  }
